package myproject;

public class MySolution {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		if (args.length < 1) {

			System.out.println("Usage: java -jar myProject-0.0.1-SNAPSHOT.jar 1 or 2");
			return;
		}
		String option = args[0];
		if (!option.equals("1") && !option.equals("2")) {

			System.out.println("Invalid Option. Usage: java -jar myProject-0.0.1-SNAPSHOT.jar 1 or 2");
			return;
		}

		MySolution sol = new MySolution();
		sol.solution(option);

	}

	public boolean checkCondition(int number, int conditionNumber, String option) {

		boolean divendBy = number % conditionNumber == 0 ? true : false;
		boolean has = false;
		if (!divendBy && option.equals("2")) {
			has = (number + "").indexOf(conditionNumber + "") != -1 ? true : false;
		}
		return divendBy || has;
	}

	public boolean isFizz(int number, String option) {

		return checkCondition(number, 3, option);
	}

	public boolean isBuzz(int number, String option) {

		return checkCondition(number, 5, option);
	}

	public boolean isFizzBuzz(int number, String option) {

		return isFizz(number, option) && isBuzz(number, option);
	}

	private void solution(String option) {

		for (int i = 1; i <= 100; i++) {
			String s = i + "";
			if (isFizzBuzz(i, option)) {

				s = "FizzBuzz";
			} else if (isFizz(i, option)) {
				s = "Fizz";
			} else if (isBuzz(i, option)) {
				s = "Buzz";
			}

			System.out.println(s);

		}

	}

}
