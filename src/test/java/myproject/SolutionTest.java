package myproject;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class SolutionTest {

	MySolution solution = new MySolution();

	@Test
	public void testCheckCondition() {

		assertTrue(solution.checkCondition(21, 3, "1"));
		assertFalse(solution.checkCondition(25, 3, "1"));
		assertTrue(solution.checkCondition(53, 3, "2"));
		assertFalse(solution.checkCondition(56, 3, "2"));

		assertTrue(solution.checkCondition(35, 5, "1"));
		assertFalse(solution.checkCondition(26, 5, "1"));
		assertTrue(solution.checkCondition(53, 5, "2"));
		assertFalse(solution.checkCondition(76, 5, "2"));

	}

	@Test
	public void testOption1() {

		assertFalse(solution.isFizz(1, "1"));
		assertFalse(solution.isFizz(25, "1"));
		assertTrue(solution.isFizz(60, "1"));
		assertFalse(solution.isFizz(100, "1"));

		assertFalse(solution.isBuzz(1, "1"));
		assertTrue(solution.isBuzz(25, "1"));
		assertTrue(solution.isBuzz(60, "1"));
		assertTrue(solution.isBuzz(100, "1"));

		assertFalse(solution.isFizzBuzz(1, "1"));
		assertFalse(solution.isFizzBuzz(25, "1"));
		assertTrue(solution.isFizzBuzz(60, "1"));
		assertFalse(solution.isFizzBuzz(100, "1"));

	}

	@Test
	public void testOption2() {

		assertFalse(solution.isFizz(1, "2"));
		assertFalse(solution.isFizz(25, "2"));
		assertTrue(solution.isFizz(23, "2"));
		assertTrue(solution.isFizz(51, "2"));
		assertTrue(solution.isFizz(60, "2"));
		assertFalse(solution.isFizz(100, "2"));

		assertFalse(solution.isBuzz(1, "2"));
		assertTrue(solution.isBuzz(25, "2"));
		assertFalse(solution.isBuzz(23, "2"));
		assertTrue(solution.isBuzz(51, "2"));
		assertTrue(solution.isBuzz(60, "2"));
		assertTrue(solution.isBuzz(100, "2"));

		assertFalse(solution.isFizzBuzz(1, "2"));
		assertFalse(solution.isFizzBuzz(25, "2"));
		assertFalse(solution.isFizzBuzz(23, "2"));
		assertTrue(solution.isFizzBuzz(51, "2"));
		assertTrue(solution.isFizzBuzz(60, "2"));
		assertFalse(solution.isFizzBuzz(100, "2"));

	}
}
